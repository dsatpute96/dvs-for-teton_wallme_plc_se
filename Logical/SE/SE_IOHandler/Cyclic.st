
PROGRAM _CYCLIC
	// Grab IO values from global registers and perform bit based mapping
//	mask := 0;
//	offset := 0;
//	// DI_1
//	FOR i:=0 TO 15 BY 1 DO
//		SETable.TBD_Left.IN [i] := BIT_TST(diSETBD_Left.TBD_Left.DI1 ,i);
//		SETable.TBD_Right.IN[i] := BIT_TST(diSETBD_Left.TBD_Right.DI1 ,i);
//	END_FOR
//		// DI_2
//	offset := SIZEOF(diSETBD_Left.TBD_Right.DI1);
//	FOR i:=0 TO 15 BY 1 DO
//		SETable.TBD_Left.IN [i+offset] := BIT_TST(diSETBD_Left.TBD_Left.DI2 ,i);
//		SETable.TBD_Right.IN[i+offset] := BIT_TST(diSETBD_Left.TBD_Right.DI2 ,i);
//	END_FOR
	
	//Table TBD_Left #1 DI1 inputs
	TableIO.TBD_Left_IN.diHomeTopLeft := diSETBD_Left.TBD_Left.DI1.0;
	TableIO.TBD_Left_IN.diHomeBottomLeft := diSETBD_Left.TBD_Left.DI1.1;
	TableIO.TBD_Left_IN.diLeftPin1Up := diSETBD_Left.TBD_Left.DI1.2;
	TableIO.TBD_Left_IN.diLeftPin1Down := diSETBD_Left.TBD_Left.DI1.3;
	TableIO.TBD_Left_IN.diLeftPin2Up := diSETBD_Left.TBD_Left.DI1.4;
    TableIO.TBD_Left_IN.diLeftPin2Down := diSETBD_Left.TBD_Left.DI1.5;
    TableIO.TBD_Left_IN.diLeftPin3Up := diSETBD_Left.TBD_Left.DI1.6;
	TableIO.TBD_Left_IN.diLeftPin3Down := diSETBD_Left.TBD_Left.DI1.7;
	//Table TBD_Left #2 DI2 inputs
	TableIO.TBD_Left_IN.diLeftPin4Up := diSETBD_Left.TBD_Left.DI1.8;
	TableIO.TBD_Left_IN.diLeftPin4Down := diSETBD_Left.TBD_Left.DI1.9;
	TableIO.TBD_Left_IN.diLeftPin5Up := diSETBD_Left.TBD_Left.DI1.10;
	TableIO.TBD_Left_IN.diLeftPin5Down := diSETBD_Left.TBD_Left.DI1.11;
	TableIO.TBD_Left_IN.diLeftPin6Up := diSETBD_Left.TBD_Left.DI1.12;
	TableIO.TBD_Left_IN.diLeftPin6Down := diSETBD_Left.TBD_Left.DI1.13;
	TableIO.TBD_Left_IN.diLeftPin7Up := diSETBD_Left.TBD_Left.DI1.14;
	TableIO.TBD_Left_IN.diLeftPin7Down := diSETBD_Left.TBD_Left.DI1.15;
	//Table TBD_Left #3 DI3 inputs
	TableIO.TBD_Left_IN.diLeftPin8Up := diSETBD_Left.TBD_Left.DI2.0;
	TableIO.TBD_Left_IN.diLeftPin8Down := diSETBD_Left.TBD_Left.DI2.1;
	TableIO.TBD_Left_IN.diLeftPin9Up := diSETBD_Left.TBD_Left.DI2.2;
	TableIO.TBD_Left_IN.diLeftPin9Up := diSETBD_Left.TBD_Left.DI2.3;

	//Table TBD_Left #1 DO1 Outputs
	TableIO.TBD_Left_OUT.doLeftPin1Ext  := doSETBD_Left.TBD_Left.DO1.0;
	TableIO.TBD_Left_OUT.doLeftPin1Ret 	:= doSETBD_Left.TBD_Left.DO1.1;
	TableIO.TBD_Left_OUT.doLeftPin2Ext  := doSETBD_Left.TBD_Left.DO1.2;
	TableIO.TBD_Left_OUT.doLeftPin2Ret 	:= doSETBD_Left.TBD_Left.DO1.3;
	TableIO.TBD_Left_OUT.doLeftPin3Ext  := doSETBD_Left.TBD_Left.DO1.4;
	TableIO.TBD_Left_OUT.doLeftPin3Ret 	:= doSETBD_Left.TBD_Left.DO1.5;
	TableIO.TBD_Left_OUT.doLeftPin4Ext  := doSETBD_Left.TBD_Left.DO1.6;
	TableIO.TBD_Left_OUT.doLeftPin4Ret 	:= doSETBD_Left.TBD_Left.DO1.7;
	TableIO.TBD_Left_OUT.doLeftPin5Ext  := doSETBD_Left.TBD_Left.DO1.8;
	TableIO.TBD_Left_OUT.doLeftPin5Ret 	:= doSETBD_Left.TBD_Left.DO1.9;
	TableIO.TBD_Left_OUT.doLeftPin6Ext  := doSETBD_Left.TBD_Left.DO1.10;
	TableIO.TBD_Left_OUT.doLeftPin6Ret 	:= doSETBD_Left.TBD_Left.DO1.11;
	TableIO.TBD_Left_OUT.doLeftPin7Ext  := doSETBD_Left.TBD_Left.DO1.12;
	TableIO.TBD_Left_OUT.doLeftPin7Ret 	:= doSETBD_Left.TBD_Left.DO1.13;
	TableIO.TBD_Left_OUT.doLeftPin8Ext  := doSETBD_Left.TBD_Left.DO1.14;
	TableIO.TBD_Left_OUT.doLeftPin8Ret 	:= doSETBD_Left.TBD_Left.DO1.15;
	//Table TBD_Left #2 DO2 Outputs
	TableIO.TBD_Left_OUT.doLeftPin9Ext  := doSETBD_Left.TBD_Left.DO2.0;
	TableIO.TBD_Left_OUT.doLeftPin9Ret 	:= doSETBD_Left.TBD_Left.DO2.1;
	TableIO.TBD_Left_OUT.doLeftPin10Ext  := doSETBD_Left.TBD_Left.DO2.2;
	TableIO.TBD_Left_OUT.doLeftPin10Ret := doSETBD_Left.TBD_Left.DO2.3;
	
	
	
	

END_PROGRAM