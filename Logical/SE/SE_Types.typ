
TYPE
	diTable_IO_Type : 	STRUCT 
		IN : ARRAY[0..31]OF BOOL;
		OUT : ARRAY[0..31]OF BOOL;
	END_STRUCT;
	SE_Table_IO_Type : 	STRUCT 
		R1_6700_IN : Table_6700_IN;
		R1_6700_OUT : Table_6700_OUT;
		R2_4600_IN : Table_4600_IN;
		R2_4600_OUT : Table_4600_OUT;
		R3_4600_IN : Table_4600_IN;
		R3_4600_OUT : Table_4600_OUT;
		TBD_Left_IN : Table_TBD_Left_IN;
		TBD_Left_OUT : Table_TBD_Left_OUT;
		TBD_Right_IN : Table_TBD_Right_IN;
		TBD_Right_OUT : Table_TBD_Right_OUT;
		R1_6700 : diTable_IO_Type;
	END_STRUCT;
	SE_Table_Type : 	STRUCT 
		TBD_Left : diTable_IO_Type;
		R2_4600 : diTable_IO_Type;
		R3_4600 : diTable_IO_Type;
		TBD_Right : diTable_IO_Type;
	END_STRUCT;
	Table_TBD_Right_IN : 	STRUCT 
		diHomeBottomRight : BOOL;
		diHomeTopRight : BOOL;
		diRightPin1Down : BOOL;
		diRightPin1Up : BOOL;
		diRightPin2Down : BOOL;
		diRightPin2Up : BOOL;
		diRightPin3Down : BOOL;
		diRightPin3Up : BOOL;
		diRightPin4Down : BOOL;
		diRightPin4Up : BOOL;
		diRightPin5Down : BOOL;
		diRightPin5Up : BOOL;
		diRightPin6Down : BOOL;
		diRightPin6Up : BOOL;
		diRightPin7Down : BOOL;
		diRightPin7Up : BOOL;
		diRightPin8Down : BOOL;
		diRightPin8Up : BOOL;
		diRightPin9Down : BOOL;
		diRightPin9Up : BOOL;
		diRightPin10Down : BOOL;
		diRightPin10Up1 : BOOL;
		diRightPin1Home : BOOL;
		diRightPin2Home : BOOL;
		diRightPin3Home : BOOL;
		diRightPin4Home : BOOL;
		diRightPin5Home : BOOL;
		diRightPin6Home : BOOL;
		diRightPin7Home : BOOL;
		diRightPin8Home : BOOL;
		diRightPin9Home : BOOL;
		diRightPin10Home : BOOL;
	END_STRUCT;
	Table_TBD_Right_OUT : 	STRUCT 
		doRightPin1Ext : BOOL;
		doRightPin1Ret : BOOL;
		doRightPin2Ext : BOOL;
		doRightPin2Ret : BOOL;
		doRightPin3Ext : BOOL;
		doRightPin3Ret : BOOL;
		doRightPin4Ext : BOOL;
		doRightPin4Ret : BOOL;
		doRightPin5Ext : BOOL;
		doRightPin5Ret : BOOL;
		doRightPin6Ext : BOOL;
		doRightPin6Ret : BOOL;
		doRightPin7Ext : BOOL;
		doRightPin7Ret : BOOL;
		doRightPin8Ext : BOOL;
		doRightPin8Ret : BOOL;
		doRightPin9Ext : BOOL;
		doRightPin9Ret : BOOL;
		doRightPin10Ext : BOOL;
		doRightPin10Ret : BOOL;
	END_STRUCT;
	Table_TBD_Left_OUT : 	STRUCT 
		doLeftPin1Ext : BOOL;
		doLeftPin1Ret : BOOL;
		doLeftPin2Ext : BOOL;
		doLeftPin2Ret : BOOL;
		doLeftPin3Ext : BOOL;
		doLeftPin3Ret : BOOL;
		doLeftPin4Ext : BOOL;
		doLeftPin4Ret : BOOL;
		doLeftPin5Ext : BOOL;
		doLeftPin5Ret : BOOL;
		doLeftPin6Ext : BOOL;
		doLeftPin6Ret : BOOL;
		doLeftPin7Ext : BOOL;
		doLeftPin7Ret : BOOL;
		doLeftPin8Ext : BOOL;
		doLeftPin8Ret : BOOL;
		doLeftPin9Ext : BOOL;
		doLeftPin9Ret : BOOL;
		doLeftPin10Ext : BOOL;
		doLeftPin10Ret : BOOL;
	END_STRUCT;
	Table_4600_OUT : 	STRUCT 
		doGunPressureOn : BOOL; (*External Valve - Nail Gun Pressure*)
		doReleaseTool : BOOL; (*Tool Changer Release*)
		doAttachTool : BOOL; (*Tool Changer Attach*)
		doExtendGripper1 : BOOL; (*Grip 1 Extend*)
		doRetractGripper1 : BOOL; (*Grip 1 Retract*)
		doExtendGripper2 : BOOL; (*Grip 2 Extend*)
		doRetractGripper2 : BOOL; (*Grip 2 Retract*)
		doExtendGripper3 : BOOL; (*Grip 3 Extend*)
		doRetractGripper3 : BOOL; (*Grip 3 Retract*)
		doExtendGripper4 : BOOL; (*Grip 4 Extend*)
		doRetractGripper4 : BOOL; (*Grip 4 Retract*)
		doTriggerNail : BOOL; (*Nail Gun Trigger*)
	END_STRUCT;
	Table_6700_OUT : 	STRUCT 
		doBoardVacR1 : BOOL;
		doBoardBLR1 : BOOL;
		doBoardVacR2 : BOOL;
		doBoardBLR2 : BOOL;
		doBoardVacR3 : BOOL;
		doBoardBLR3 : BOOL;
		doBoardVacR4 : BOOL;
		doBoardBLR4 : BOOL;
		doBoardVacL1 : BOOL;
		doBoardBLL1 : BOOL;
		doBoardVacL2 : BOOL;
		doBoardBLL2 : BOOL;
		doBoardVacL3 : BOOL;
		doBoardBLL3 : BOOL;
		doBoardVacL4 : BOOL;
		doBoardBLL4 : BOOL;
	END_STRUCT;
	Table_6700_IN : 	STRUCT 
		diBoardPresent1 : BOOL;
		diBoardPresent2 : BOOL;
		diBoardPresent3 : BOOL;
		diBoardPresent4 : BOOL;
	END_STRUCT;
	Table_4600_IN : 	STRUCT 
		diBoardPresent1 : BOOL;
		diBoardPresent2 : BOOL;
		diGripper1Open : BOOL;
		diGripper1Closed : BOOL;
		diGripper2Open : BOOL;
		diGripper2Closed : BOOL;
		diGripper3Open : BOOL;
		diGripper3Closed : BOOL;
		diGripper4Open : BOOL;
		diGripper4Closed : BOOL;
	END_STRUCT;
	Fault : 	STRUCT 
		DownTotalFault : BOOL;
		UpTotalFault : BOOL;
		TotalFault : BOOL;
	END_STRUCT;
	Table_TBD_Left_IN : 	STRUCT 
		diHomeBottomLeft : BOOL;
		diHomeTopLeft : BOOL;
		diLeftPin1Down : BOOL;
		diLeftPin1Up : BOOL;
		diLeftPin2Down : BOOL;
		diLeftPin2Up : BOOL;
		diLeftPin3Down : BOOL;
		diLeftPin3Up : BOOL;
		diLeftPin4Down : BOOL;
		diLeftPin4Up : BOOL;
		diLeftPin5Down : BOOL;
		diLeftPin5Up : BOOL;
		diLeftPin6Down : BOOL;
		diLeftPin6Up : BOOL;
		diLeftPin7Down : BOOL;
		diLeftPin7Up : BOOL;
		diLeftPin8Down : BOOL;
		diLeftPin8Up : BOOL;
		diLeftPin9Down : BOOL;
		diLeftPin9Up : BOOL;
		diLeftPin10Down : BOOL;
		diLeftPin10Up : BOOL;
		diLeftPin1Home : BOOL;
		diLeftPin2Home : BOOL;
		diLeftPin3Home : BOOL;
		diLeftPin4Home : BOOL;
		diLeftPin5Home : BOOL;
		diLeftPin6Home : BOOL;
		diLeftPin7Home : BOOL;
		diLeftPin8Home : BOOL;
		diLeftPin9Home : BOOL;
		diLeftPin10Home : BOOL;
	END_STRUCT;
END_TYPE
